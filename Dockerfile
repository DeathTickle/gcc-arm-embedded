FROM ubuntu:latest

RUN apt-get update && apt-get -y install software-properties-common

RUN add-apt-repository ppa:team-gcc-arm-embedded/ppa

RUN apt-get update && apt-get -y install gcc-arm-embedded make
